<?php

namespace App\Http\Controllers;

use App\Events\StatusLiked;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;

class UserController extends Controller
{
    public function openForm(){
        return view('openForm');
    }

    public function submittForm(Request $request){

        $text=$request->text;
        event(new StatusLiked($text));
        return back();
        
    }

}
