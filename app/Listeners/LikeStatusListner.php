<?php

namespace App\Listeners;

use App\Events\StatusLiked;
use App\Models\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class LikeStatusListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StatusLiked $event)
    {
       $notification=new Notification();
       $notification->type="App\Events\StatusLiked";
       $notification->notifiable_type="App\Models\User";
       $notification->notifiable_id=1;
       $notification->data=$event->text;
       $notification->save();
    }
}
