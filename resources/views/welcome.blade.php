<!DOCTYPE html>

<head>
    <title>Pusher Test</title>

</head>

<body>
    <h1>Pusher Test</h1>
    <p>
        Try publishing an event to channel <code>my-channel</code>
        with event name <code>status-liked</code>.
    </p>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('8a6914ea2072261260af', {
        cluster: 'ap2'
    });

    var channel = pusher.subscribe('my-channel');

    channel.bind('status-liked', function(data) {
        alert(JSON.stringify(data['text']) + ' registered Successfully');
    });
</script>

</html>
